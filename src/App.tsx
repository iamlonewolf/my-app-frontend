import { useDispatch, useSelector } from "react-redux";
import { lazy, Fragment, Suspense, useEffect } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/scss/main.scss";
import "./styles/App.scss";
import Headerbar from "./components/Headerbar";
import Sidebar from "./components/Sidebar";
import { getAuthIdentity } from "./selectors/auth";
import { authIdentity } from "./store/auth";
import Loading from "./common/loading";
import { getConfig } from "./config";

import {
  DASHBOARD_ROOT,
  AUTH_ROOT,
  BOOKMARK_ROOT,
  CATEGORY_ROOT,
  PROFILE_ROOT,
  NOT_FOUND_ROOT,
} from "./routes";

const bookmarkApp = lazy(() => import("./components/bookmark-app/BookmarkApp"));
const categoryApp = lazy(() => import("./components/category-app/CategoryApp"));
const dashboardApp = lazy(
  () => import("./components/dashboard-app/DashboardApp")
);
const notFound = lazy(() => import("./components/NotFound"));
const authenticationApp = lazy(
  () => import("./components/authentication-app/AuthenticationApp")
);
const profileApp = lazy(() => import("./components/profile-app/ProfileApp"));

function App() {
  const identity = useSelector(getAuthIdentity);
  const dispatch = useDispatch();
  console.log("from main App", identity);

  useEffect(() => {
    dispatch(authIdentity());
  }, [dispatch]);

  return (
    <Fragment>
      <Headerbar />
      <div className="container-fluid h-100 main-wrap">
        <div className="row h-100">
          <Sidebar />
          <main className="col-md-9 ms-sm-auto col-lg-10 px-md-4">
            <Suspense fallback={<Loading className="mt-4" />}>
              <Switch>
                <Route path={BOOKMARK_ROOT} component={bookmarkApp} />
                <Route path={CATEGORY_ROOT} component={categoryApp} />
                <Route path={DASHBOARD_ROOT} component={dashboardApp} />
                <Route path={PROFILE_ROOT} component={profileApp} />
                <Route path={AUTH_ROOT} component={authenticationApp} />

                <Route path={NOT_FOUND_ROOT} component={notFound} />
                <Redirect from="/" exact to="/dashboard" />
                <Redirect to={NOT_FOUND_ROOT} />
              </Switch>
            </Suspense>
          </main>
        </div>
      </div>
      <ToastContainer />
    </Fragment>
  );
}

export default App;
