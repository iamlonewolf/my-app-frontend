import { createSelector } from "@reduxjs/toolkit";
import { RootState } from "../models/interfaces/rootState";
import { AppState } from "../models/interfaces/appState";

export const getBookmarksEntities = createSelector(
  ({ entities }: RootState): AppState | undefined => entities?.bookmark,
  (bookmark: AppState | undefined) => {
    return bookmark;
  }
);

export const getBookmark = createSelector(
  getBookmarksEntities,
  (bookmark: AppState | undefined) => {
    return bookmark?.data;
  }
);

export const getBookmarks = createSelector(
  getBookmarksEntities,
  (bookmark: AppState | undefined) => {
    return bookmark?.list;
  }
);

export const isLoading = createSelector(
  getBookmarksEntities,
  (bookmark: AppState | undefined) => {
    return bookmark?.loading;
  }
);

export const getBookmarkError = createSelector(
  getBookmarksEntities,
  (bookmark: AppState | undefined) => {
    return bookmark?.error;
  }
);

export const getBookmarkListCount = createSelector(
  getBookmarksEntities,
  (bookmark: AppState | undefined) => {
    return bookmark?.count ?? 0;
  }
);
