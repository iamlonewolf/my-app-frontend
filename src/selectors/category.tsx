import { createSelector } from "@reduxjs/toolkit";
import { AppState } from "../models/interfaces/appState";
import { RootState } from "../models/interfaces/rootState";

export const getCategoryEntities = createSelector(
  ({ entities }: RootState): AppState | undefined => entities?.category,
  (category: AppState | undefined) => {
    return category;
  }
);

export const getCategories = createSelector(
  getCategoryEntities,
  (category: AppState | undefined) => {
    return category?.list;
  }
);

export const getCategoriesCount = createSelector(
  getCategoryEntities,
  (category: AppState | undefined) => {
    return category?.count ?? 0;
  }
);

export const getCategoryError = createSelector(
  getCategoryEntities,
  (category: AppState | undefined) => {
    return category?.error;
  }
);

export const isProcessing = createSelector(
  getCategoryEntities,
  (category: AppState | undefined) => {
    return category?.loading;
  }
);
