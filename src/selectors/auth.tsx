import { createSelector } from "@reduxjs/toolkit";
import { isUndefined } from "lodash-es";
import { RootState } from "../models/interfaces/rootState";
import { AuthState } from "../models/interfaces/authState";

export const getAuth = ({ auth }: RootState) => auth?.identity;

export const isAuthProcessing = ({ auth }: RootState) => auth?.loading;

export const getAuthError = ({ auth }: RootState) => auth?.error;

export const getAuthIdentity = createSelector(
  ({ auth }: RootState): AuthState | undefined => auth,
  (auth: AuthState | undefined) => {
    if (!auth) {
      return undefined;
    }
    return auth.identity;
  }
);

export const getAccountId = createSelector(getAuthIdentity, (identity) => {
  return identity?._id;
});

export const hasIdentity = (state: RootState) => {
  return !isUndefined(getAuthIdentity(state));
};
