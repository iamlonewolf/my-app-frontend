import { useEffect, FC } from "react";
import { useForm } from "react-hook-form";
import { withRouter } from "react-router-dom";
import { joiResolver } from "@hookform/resolvers/joi";
import { isUndefined } from "lodash-es";
import { useDispatch, useSelector } from "react-redux";
import { authReset, register as registerUser } from "../../../store/auth";
import { RegistrationData } from "./../../../models/interfaces/registration";
import Loading from "../../loading";
import { isAuthProcessing, getAuthError } from "./../../../selectors/auth";
import { RouteComponentProps } from "react-router-dom";
import {
  accountSchema,
  DEFAULT_MIN_LENGTH,
} from "../../../models/schemas/account";

export interface RegistrationProps extends RouteComponentProps<any> {
  redirect?: string;
}

const Registration: FC<RegistrationProps> = ({ redirect, history }) => {
  const dispatch = useDispatch();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: joiResolver(accountSchema),
  });

  const onSubmit = handleSubmit(async (data: RegistrationData) => {
    await dispatch(registerUser(data));
  });

  const processing = useSelector(isAuthProcessing);
  const error = useSelector(getAuthError);

  useEffect(() => {
    dispatch(authReset());
  }, [dispatch]);

  return (
    <form onSubmit={onSubmit}>
      <div className="auth-form pt-3">
        {!isUndefined(error) && (
          <div className="alert alert-danger mb-0 py-1 mb-2">{error}</div>
        )}
        <div className="mb-2">
          <label htmlFor="name" className="form-label fw-bold">
            Name
          </label>
          <input
            readOnly={processing}
            className="form-control mb-1"
            {...register("name", {
              required: true,
              minLength: DEFAULT_MIN_LENGTH,
            })}
          />
          {errors?.name?.type && (
            <span className="text-danger text-error">
              {errors?.name?.type === "string.empty" && "Name is required."}
              {errors?.name?.type === "string.min" &&
                "Name should not be less than 5"}
            </span>
          )}
        </div>
        <div className="mb-2">
          <label htmlFor="email" className="form-label fw-bold">
            Email
          </label>
          <input
            type="email"
            readOnly={processing}
            className="form-control mb-1"
            {...register("email", {
              required: true,
              pattern: {
                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                message: "Enter a valid e-mail address",
              },
            })}
          />
          {errors?.email?.type && (
            <span className="text-danger text-error">
              {errors?.email?.type === "string.empty" && "Email is required."}
              {errors?.email?.type === "string.email" && "Email is invalid."}
              {errors?.email?.type === "email.unique" && "Email already exist."}
            </span>
          )}
        </div>
        <div className="mb-3">
          <label htmlFor="password" className="form-label fw-bold">
            Password
          </label>
          <input
            type="password"
            readOnly={processing}
            className="form-control"
            {...register("password", {
              required: true,
              minLength: DEFAULT_MIN_LENGTH,
            })}
          />
          {errors?.password?.type && (
            <span className="text-danger text-error">
              {errors?.password?.type === "string.empty" &&
                "Password is required."}
              {errors?.password?.type === "string.min" &&
                "Password should not be less than 4."}
            </span>
          )}
        </div>
        <button
          type="submit"
          className="btn btn-action w-100"
          disabled={processing}
        >
          {processing && <Loading />} Submit
        </button>
      </div>
    </form>
  );
};

export default withRouter(Registration);
