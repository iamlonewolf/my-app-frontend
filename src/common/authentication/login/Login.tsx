import "../Authentication.scss";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useForm } from "react-hook-form";
import { joiResolver } from "@hookform/resolvers/joi";
import { isUndefined } from "lodash-es";
import {
  loginSchema,
  DEFAULT_MIN_LENGTH,
} from "../../../models/schemas/account";
import { authReset, login as loginAccount } from "./../../../store/auth";
import { useEffect } from "react";
import { getAuthError, isAuthProcessing } from "../../../selectors/auth";
import { LoginData } from "../../../models/interfaces/login";
import Loading from "../../loading";

export interface LoginAppProps extends RouteComponentProps<any> {
  redirect?: string;
}
const LoginApp: React.FC<LoginAppProps> = ({ redirect, history }) => {
  const dispatch = useDispatch();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: joiResolver(loginSchema),
  });

  const onSubmit = handleSubmit((data: LoginData) => {
    dispatch(loginAccount(data));
  });

  const processing = useSelector(isAuthProcessing);
  const error = useSelector(getAuthError);

  useEffect(() => {
    dispatch(authReset());
  }, [dispatch]);

  return (
    <form onSubmit={onSubmit}>
      <div className="auth-form pt-3">
        {!isUndefined(error) && (
          <div className="alert alert-danger mb-0 py-1 mb-2">{error}</div>
        )}
        <div className="mb-3">
          <label htmlFor="password" className="form-label fw-bold">
            Email:
          </label>
          <input
            type="email"
            readOnly={processing}
            className="form-control mb-1"
            {...register("email", {
              required: true,
              pattern: {
                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                message: "Enter a valid e-mail address",
              },
            })}
          />
          {errors?.email?.type && (
            <span className="text-danger text-error">
              {errors?.email?.type === "string.empty" && "Email is required."}
              {errors?.email?.type === "string.email" && "Email is invalid."}
            </span>
          )}
        </div>
        <div className="mb-3">
          <div className="mb-3">
            <label htmlFor="password" className="form-label fw-bold">
              Password:
            </label>
            <input
              type="password"
              readOnly={processing}
              className="form-control"
              {...register("password", {
                required: true,
                minLength: DEFAULT_MIN_LENGTH,
              })}
            />
            {errors?.password?.type && (
              <span className="text-danger text-error">
                {errors?.password?.type === "string.empty" &&
                  "Password is required."}
                {errors?.password?.type === "string.min" &&
                  "Password should not be less than 4."}
              </span>
            )}
          </div>
        </div>
        <button
          type="submit"
          className="btn btn-action w-100"
          disabled={processing}
        >
          {processing && <Loading />} Submit
        </button>
      </div>
    </form>
  );
};

export default withRouter(LoginApp);
