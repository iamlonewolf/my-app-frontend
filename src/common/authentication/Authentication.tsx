import { FC, Fragment } from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import Login from "./login/Login";
import Registration from "./registration/Registration";

export interface AuthenticationAppProps extends RouteComponentProps<any> {
  mode: AuthenticationMode;
  redirect?: string;
}

export enum AuthenticationMode {
  LOGIN = "login",
  REGISTRATION = "register",
}

const Authentication: FC<AuthenticationAppProps> = ({ mode, redirect }) => {
  if (!mode) {
    return null;
  }
  return (
    <Fragment>
      <div>
        <h3 className="mb-0">
          <i className="fas fa-user-lock me-2"></i>
          {mode === AuthenticationMode.LOGIN && <span>Login here</span>}
          {mode === AuthenticationMode.REGISTRATION && (
            <span>Register here</span>
          )}
        </h3>
        {mode === AuthenticationMode.LOGIN && <Login redirect={redirect} />}
        {mode === AuthenticationMode.REGISTRATION && (
          <Registration redirect={redirect} />
        )}
      </div>
    </Fragment>
  );
};

export default withRouter(Authentication);
