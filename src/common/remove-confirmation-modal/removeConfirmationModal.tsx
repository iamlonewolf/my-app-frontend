import { FC, Fragment } from "react";
import { Button, Modal } from "react-bootstrap";
import Loading, { LoadingSize } from "../loading";
import "./removeConfirmationModal.scss";

export interface RemoveConfirmationModalProps {
  show?: boolean;
  loading?: boolean;
  centered?: boolean;
  onSubmit?: () => void;
  onClose?: () => void;
}

const RemoveConfirmationModal: FC<RemoveConfirmationModalProps> = ({
  show,
  centered,
  loading,
  onSubmit,
  onClose,
}: RemoveConfirmationModalProps) => {
  return (
    <Modal className="alert-remove" show={show} centered={centered}>
      <Modal.Header className="py-2">
        <Modal.Title className="h5">Confirm Delete</Modal.Title>
      </Modal.Header>
      <Modal.Body className="d-flex justify-content-center py-3">
        {loading === true && <Loading size={LoadingSize.MEDIUM}></Loading>}
        {!loading && <Fragment>Are you sure you want to delete?</Fragment>}
      </Modal.Body>
      {!loading && (
        <Modal.Footer className="py-2">
          <Button
            className="btn-action"
            variant="default"
            onClick={() => (onSubmit ? onSubmit() : () => {})}
          >
            Remove
          </Button>
          <Button
            className="btn-action"
            variant="default"
            onClick={() => (onClose ? onClose() : () => {})}
          >
            Close
          </Button>
        </Modal.Footer>
      )}
    </Modal>
  );
};

export default RemoveConfirmationModal;
