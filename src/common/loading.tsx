import { FC } from "react";

export enum LoadingSize {
  SMALL = 1,
  MEDIUM = 2,
  LARGE = 3,
}
export interface LoadingProps {
  size?: LoadingSize;
}

const Loading: FC<LoadingProps & React.HTMLAttributes<HTMLDivElement>> = ({
  className,
  size,
}) => {
  const getSizeClass = (): string => {
    if (size === LoadingSize.MEDIUM) {
      return "fa-2x";
    }
    if (size === LoadingSize.LARGE) {
      return "fa-3x";
    }
    return "fa-1x";
  };
  return (
    <i className={`fas fa-sync fa-spin ${className} ${getSizeClass()}`}></i>
  );
};

export default Loading;
