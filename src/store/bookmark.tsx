import { createSlice } from "@reduxjs/toolkit";
import { Bookmark } from "../models/interfaces/bookmark";
import { AppState } from "../models/interfaces/appState";
import { apiCall } from "./api2";

const slice = createSlice({
  name: "bookmarks",
  initialState: {
    loading: false,
    list: [],
    data: undefined,
    error: undefined,
    count: 0,
  },
  reducers: {
    bookmarkRequestStarted: (bookmark: AppState) => {
      bookmark.loading = true;
      console.log("bookmark request started.");
    },

    bookmarkRequestDone: (bookmark: AppState) => {
      bookmark.loading = false;
      console.log("bookmark request done.");
    },

    bookmarkSubmitFailed: (bookmark, { payload }) => {
      bookmark.error = payload;
      console.log("BOOKMARK REQUEST FAILED!");
    },

    // reset form values
    bookmarkReset: (bookmark) => {
      bookmark.error = undefined;
      bookmark.data = undefined;
      console.log("bookmark reset");
    },

    //Bookmark list
    bookmarkListLoaded: (bookmark, { payload }) => {
      bookmark.list = payload?.data;
      bookmark.count = payload?.headers?.count;
    },

    //Bookmark count list
    bookmarkListCountLoaded: (bookmark, { payload }) => {
      bookmark.count = payload?.data;
      console.log(bookmark.count);
    },

    bookmarkLoadFailed: (bookmark, { payload }) => {},

    // Bookmark Form
    bookmarkLoaded: (bookmark, { payload }) => {
      bookmark.data = payload?.data;
    },
  },
});

export const loadBookmarks = (
  accountId: string | undefined,
  params: object
) => {
  return apiCall({
    url: `/bookmark/account/${accountId}`,
    method: "GET",
    onStart: bookmarkRequestStarted.type,
    onSuccess: bookmarkListLoaded.type,
    onError: bookmarkLoadFailed.type,
    onDone: bookmarkRequestDone.type,
    queryParams: params,
  });
};

export const loadBookmarksCount = (
  accountId: string | undefined,
  params: object
) => {
  return apiCall({
    url: `/bookmark/account/${accountId}/count`,
    method: "GET",
    onStart: bookmarkRequestStarted.type,
    onSuccess: bookmarkListCountLoaded.type,
    onError: bookmarkLoadFailed.type,
    onDone: bookmarkRequestDone.type,
    queryParams: params,
  });
};

export const loadBookmark = (bookmarkId: string | undefined) => {
  return apiCall({
    url: `/bookmark/${bookmarkId}`,
    method: "GET",
    onStart: bookmarkRequestStarted.type,
    onSuccess: bookmarkLoaded.type,
    onError: bookmarkLoadFailed.type,
    onDone: bookmarkRequestDone.type,
  });
};

export const addBookmark = (bookmark: Bookmark) => {
  return apiCall({
    url: "/bookmark",
    method: "POST",
    onDone: bookmarkRequestDone.type,
    onError: bookmarkSubmitFailed.type,
    postParams: bookmark,
  });
};

export const updateBookmark = (bookmarkId: string, bookmark: Bookmark) => {
  return apiCall({
    url: `/bookmark/${bookmarkId}`,
    method: "PUT",
    onDone: bookmarkRequestDone.type,
    onError: bookmarkSubmitFailed.type,
    postParams: bookmark,
  });
};

export const deleteBookmark = (bookmarkCategoryId: string | undefined) => {
  return apiCall({
    url: `/bookmark/${bookmarkCategoryId}`,
    method: "DELETE",
    onStart: bookmarkRequestStarted.type,
    onDone: bookmarkRequestDone.type,
    onError: bookmarkSubmitFailed.type,
  });
};
export const {
  bookmarkRequestStarted,
  bookmarkLoaded,
  bookmarkListLoaded,
  bookmarkListCountLoaded,
  bookmarkLoadFailed,
  bookmarkSubmitFailed,
  bookmarkRequestDone,
  bookmarkReset,
} = slice.actions;

export default slice.reducer;
