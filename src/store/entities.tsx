import { combineReducers } from "redux";
import bookmarkReducer from "./bookmark";
import categoryReducer from "./category";
import accountReducer from "./account";
export default combineReducers({
  bookmark: bookmarkReducer,
  category: categoryReducer,
  account: accountReducer,
});
