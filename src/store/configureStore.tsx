import { configureStore } from "@reduxjs/toolkit";
import api from "../middleware/api";
import reducer from "./reducer";
// export default function initializeStore() {
//   return configureStore({
//     reducer,
//     middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(api),
//   });
// }
export const store = configureStore({
  reducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(api),
});

export type AppDispatch = typeof store.dispatch;
