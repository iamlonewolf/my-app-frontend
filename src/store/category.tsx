import { createSlice } from "@reduxjs/toolkit";
import { apiCall } from "./api2";
import { AppState } from "../models/interfaces/appState";
import { Category } from "../models/interfaces/category";

const slice = createSlice({
  name: "category",
  initialState: {
    list: [],
    count: 0,
    data: undefined,
    loading: false,
  },
  reducers: {
    categoryRequested: (category: AppState, { payload }) => {
      category.loading = true;
    },
    categoryListLoaded: (category: AppState, { payload }) => {
      category.list = payload.data;
      category.count = payload.headers?.count;
    },
    categoryLoaded: (category: AppState, { payload }) => {
      category.data = payload.data;
    },
    categoryLoadFailed: (category: AppState, { payload }) => {
      category.list = [];
    },
    categoryRequestDone: (category: AppState, { payload }) => {
      category.loading = false;
    },
  },
});

export const loadAccountCategories = (
  accountId: string | undefined,
  params?: object
) => {
  return apiCall({
    url: `/category/account/${accountId}`,
    method: "GET",
    onStart: categoryRequested.type,
    onSuccess: categoryListLoaded.type,
    onError: categoryLoadFailed.type,
    onDone: categoryRequestDone.type,
    queryParams: params,
  });
};

export const loadCategory = (bookmarkId: string | undefined) => {
  return apiCall({
    url: `/category/${bookmarkId}`,
    method: "GET",
    onStart: categoryRequested.type,
    onSuccess: categoryLoaded.type,
    onError: categoryLoadFailed.type,
    onDone: categoryRequestDone.type,
  });
};

export const addCategory = (bookmark: Category) => {
  return apiCall({
    url: "/category",
    method: "POST",
    onStart: categoryRequested.type,
    onDone: categoryRequestDone.type,
    onError: categoryLoadFailed.type,
    postParams: bookmark,
  });
};

export const updateCategory = (categoryId: string, category: Category) => {
  return apiCall({
    url: `/category/${categoryId}`,
    method: "PUT",
    onStart: categoryRequested.type,
    onDone: categoryRequestDone.type,
    onError: categoryLoadFailed.type,
    postParams: category,
  });
};

export const deleteCategory = (categoryId: string | undefined) => {
  return apiCall({
    url: `/category/${categoryId}`,
    method: "DELETE",
    onStart: categoryRequested.type,
    onDone: categoryRequestDone.type,
    onError: categoryLoadFailed.type,
  });
};

export const {
  categoryRequested,
  categoryListLoaded,
  categoryLoaded,
  categoryLoadFailed,
  categoryRequestDone,
} = slice.actions;

export default slice.reducer;
