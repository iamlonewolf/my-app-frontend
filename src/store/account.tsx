import { createSlice } from "@reduxjs/toolkit";

const slice = createSlice({
  name: "accounts",
  initialState: {
    list: [],
  },
  reducers: {
    accountsLoaded: (accounts, { payload }) => {},
  },
});

export const { accountsLoaded } = slice.actions;
export default slice.reducer;
