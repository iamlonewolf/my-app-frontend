import { FC } from "react";

export interface ProfileAppProps {}

const ProfileApp: FC<ProfileAppProps> = () => {
  return (
    <div className="bookmarker-app">
      <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 className="h4">Profile</h1>
      </div>
    </div>
  );
};

export default ProfileApp;
