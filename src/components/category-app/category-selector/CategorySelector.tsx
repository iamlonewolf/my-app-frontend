import { FC, Fragment, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCategories } from "../../../selectors/category";
import { loadAccountCategories } from "../../../store/category";
import { AppDispatch } from "../../../store/configureStore";
import { getCategoriesCount } from "./../../../selectors/category";

export interface CategorySelectorProps {
  categoryId?: string;
  accountId?: string;
  onSelect?: (event: any) => void;
}

const CategorySelector: FC<
  CategorySelectorProps & React.HTMLAttributes<HTMLDivElement>
> = ({ categoryId, accountId, onSelect, className }) => {
  const categories = useSelector(getCategories);
  const categoriesCount = useSelector(getCategoriesCount);
  const dispatch = useDispatch<AppDispatch>();
  useEffect(() => {
    dispatch(loadAccountCategories(accountId));
    console.log("categoryId", categoryId);
  }, []);

  return (
    <Fragment>
      {categoriesCount > 0 && (
        <select
          value={categoryId}
          onChange={(event) =>
            onSelect ? onSelect(event.target.value) : () => {}
          }
          className={`form-select ${className}`}
        >
          <option value="">-Select-</option>
          {categories?.map((category) => (
            <option value={category?._id} key={category._id}>
              {category?.name}
            </option>
          ))}
        </select>
      )}
    </Fragment>
  );
};

export default CategorySelector;
