import "./CategoryApp.scss";
import { Fragment, lazy } from "react";
import { useSelector } from "react-redux";
import {
  Link,
  Redirect,
  Route,
  RouteComponentProps,
  Switch,
} from "react-router-dom";
import {
  AUTH_LOGIN,
  CATEGORY_ADD,
  CATEGORY_EDIT,
  CATEGORY_ROOT,
} from "../../routes";
import { hasIdentity } from "../../selectors/auth";
import CategoryListFilter from "./category-list-filter/CategoryListFilter";
const categorForm = lazy(
  () => import("../category-app/category-form/CategoryForm")
);
const categoryList = lazy(
  () => import("../category-app/category-list/CategoryList")
);
export interface CategoryAppProps extends RouteComponentProps<any> {}

const CategoryApp: React.SFC<CategoryAppProps> = () => {
  const isLoggedIn = useSelector(hasIdentity);
  return (
    <Fragment>
      {!isLoggedIn && <Redirect to={AUTH_LOGIN} />}
      <div className="category-app">
        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
          <h1 className="h4 d-flex">
            <Link to={CATEGORY_ROOT} className="text-dark">
              Categories
            </Link>
          </h1>
          <div>
            <CategoryListFilter />
          </div>
        </div>
        <Switch>
          <Route path={CATEGORY_EDIT} exact component={categorForm} />
          <Route path={CATEGORY_ADD} exact component={categorForm} />
          <Route path={CATEGORY_ROOT} exact component={categoryList} />
          <Redirect to={CATEGORY_ROOT} />
        </Switch>
      </div>
    </Fragment>
  );
};

export default CategoryApp;
