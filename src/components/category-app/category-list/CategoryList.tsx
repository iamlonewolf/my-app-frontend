import "./CategoryList.scss";
import { FC, Fragment, useEffect, useState } from "react";
import { Link, RouteComponentProps } from "react-router-dom";
import queryString from "query-string";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch } from "../../../store/configureStore";
import { deleteCategory, loadAccountCategories } from "../../../store/category";
import { getCategories } from "../../../selectors/category";
import { isEmpty } from "lodash-es";
import RemoveConfirmationModal from "../../../common/remove-confirmation-modal/removeConfirmationModal";
import { Category } from "../../../models/interfaces/category";
import { toast } from "react-toastify";
import { getAccountId } from "../../../selectors/auth";
import { DEFAULT_PAGE, DEFAULT_PAGE_SIZE } from "../../../models/common";
import { valueToNumber } from "../../../utilities/num";
export interface CategoryListProps extends RouteComponentProps<any> {}
const CategoryList: FC<CategoryListProps> = ({ location }) => {
  const queryParams = queryString.parse(location.search);
  const dispatch = useDispatch<AppDispatch>();
  const categories = useSelector(getCategories);
  const accountId = useSelector(getAccountId);
  const [show, showModal] = useState(false);
  const [loading, showLoading] = useState(false);
  const [currentCategory, selectCategory] = useState<Category | undefined>(
    undefined
  );

  const initializeCategories = () => {
    const page = valueToNumber(queryParams?.page, DEFAULT_PAGE);
    let params = {
      page: page,
      pageSize: DEFAULT_PAGE_SIZE,
      searchText: queryParams?.searchText,
      category: queryParams?.category,
    };
    dispatch(loadAccountCategories(accountId, params));
  };

  const confirmRemove = async () => {
    try {
      showLoading(true);
      await dispatch(deleteCategory(currentCategory?._id)).unwrap();
      initializeCategories();
      toast.success("Category has been deleted.", {
        position: toast.POSITION.BOTTOM_RIGHT,
      });
    } catch (err) {
      toast.error("Failed deleting this category.", {
        position: toast.POSITION.BOTTOM_RIGHT,
      });
    } finally {
      showModal(false);
      showLoading(false);
    }
  };

  const remove = (category: Category) => {
    showModal(true);
    selectCategory(category);
  };

  useEffect(() => {
    initializeCategories();
  }, [queryParams?.page, queryParams?.searchText]);

  return (
    <Fragment>
      <div className="category-list col-5">
        {isEmpty(categories) && <div className="py-2">No records</div>}
        {!isEmpty(categories) && (
          <div className="col-table">
            <div className="col-head">
              <div className="col-row">
                <div className="col-item px-2">Name</div>
                <div className="col-item px-2"></div>
              </div>
            </div>
            <div className="col-body">
              {categories?.map((category) => (
                <div key={category._id} className="col-row py-2">
                  <div className="col-item d-flex align-items-center px-2">
                    {category.name}
                  </div>
                  <div className="col-item d-flex align-items-center px-2 justify-content-center">
                    <Link
                      to={`/category/${category._id}/edit`}
                      className="btn btn-sm btn-action me-2"
                    >
                      <i className="fas fa-edit"></i>
                    </Link>
                    <button
                      onClick={() => remove(category)}
                      className="btn btn-sm btn-danger btn-action"
                    >
                      <i className="fas fa-trash-alt"></i>
                    </button>
                  </div>
                </div>
              ))}
            </div>
          </div>
        )}
      </div>
      <RemoveConfirmationModal
        show={show}
        centered={true}
        loading={loading}
        onSubmit={() => confirmRemove()}
        onClose={() => showModal(false)}
      />
    </Fragment>
  );
};

export default CategoryList;
