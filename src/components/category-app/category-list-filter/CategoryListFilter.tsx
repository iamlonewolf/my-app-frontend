import "./CategoryListFilter.scss";
import { FC, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import queryString from "query-string";
import { Link, RouteComponentProps, withRouter } from "react-router-dom";
import { CATEGORY_ADD, CATEGORY_ROOT } from "../../../routes";
import { getCategoriesCount } from "./../../../selectors/category";
import { CategoryParams } from "../../../models/interfaces/params/categoryParams";
import { valueToNumber } from "../../../utilities/num";
import { DEFAULT_PAGE_SIZE } from "../../../models/common";

export interface CategoryListFilterProps extends RouteComponentProps<any> {}

const CategoryListFilter: FC<CategoryListFilterProps> = ({
  location,
  history,
}) => {
  const queryParams: CategoryParams = queryString.parse(location.search);
  const query = queryParams;
  const [searchText, setSearch] = useState("");
  const categoriesCount = useSelector(getCategoriesCount);
  const page = valueToNumber(queryParams?.page, 0);
  const totalPage = Math.ceil(categoriesCount / DEFAULT_PAGE_SIZE);

  useEffect(() => {
    setSearch(queryParams?.searchText ?? "");
  }, [queryParams?.searchText]);

  const onSubmit = (event: any) => {
    event.preventDefault();
    query.searchText = searchText;
    query.page = 0;
    history.push(`${CATEGORY_ROOT}?${queryString.stringify(query)}`);
  };

  const goPrev = () => {
    query.page = page - 1;
    history.push(`${CATEGORY_ROOT}?${queryString.stringify(query)}`);
  };

  const goNext = () => {
    query.page = page + 1;
    history.push(`${CATEGORY_ROOT}?${queryString.stringify(query)}`);
  };

  return (
    <div className="category-list-filter d-flex align-items-center">
      {categoriesCount > 0 && (
        <div className="d-flex me-2 rounded-pill px-3 py-1 category-count">
          <span className="me-2">Total:</span>
          <span>{categoriesCount}</span>
        </div>
      )}
      <Link className="btn btn-sm me-2" to={CATEGORY_ADD}>
        <i className="fas fa-plus"></i>
      </Link>
      <form
        onSubmit={(event) => onSubmit(event)}
        className="input-group input-group-sm  me-2"
      >
        <input
          type="text"
          className="form-control"
          value={searchText}
          onChange={(e) => setSearch(e.target.value)}
        />
        <button className="btn" type="submit">
          <i className="fas fa-search"></i>
        </button>
      </form>
      {categoriesCount > DEFAULT_PAGE_SIZE && (
        <div className="btn-group">
          <button
            onClick={() => goPrev()}
            className="btn btn-sm"
            disabled={!(page > 0)}
          >
            <i className="fas fa-chevron-left"></i>
          </button>
          <button
            onClick={() => goNext()}
            className="btn btn-sm"
            disabled={!(page < totalPage - 1)}
          >
            <i className="fas fa-chevron-right"></i>
          </button>
        </div>
      )}
    </div>
  );
};

export default withRouter(CategoryListFilter);
