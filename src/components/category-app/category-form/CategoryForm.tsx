import { FC, useEffect } from "react";
import { joiResolver } from "@hookform/resolvers/joi";
import { useForm } from "react-hook-form";
import { Link, RouteComponentProps, useParams } from "react-router-dom";
import {
  categorySchema,
  DEFAULT_MAX_LENGTH,
  DEFAULT_MIN_LENGTH,
} from "../../../models/schemas/category";
import { Category } from "../../../models/interfaces/category";
import {
  addCategory,
  loadCategory,
  updateCategory,
} from "../../../store/category";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch } from "../../../store/configureStore";
import { isUndefined, omit } from "lodash-es";
import { CATEGORY_ROOT } from "../../../routes";
import { getCategoryError, isProcessing } from "../../../selectors/category";
import { toast } from "react-toastify";
import Loading from "../../../common/loading";
import { notifySuccess } from "./../../../common/notify";

export interface CategoryFormProps extends RouteComponentProps<any> {}

const CategoryForm: FC<CategoryFormProps> = ({ history }) => {
  const dispatch = useDispatch<AppDispatch>();
  const { categoryId } = useParams<{ categoryId: string }>();
  const error = useSelector(getCategoryError);
  const processing = useSelector(isProcessing);
  const resolver = joiResolver(categorySchema);
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({ resolver });
  const onSubmit = handleSubmit(async (data: Category) => {
    try {
      if (categoryId) {
        await dispatch(updateCategory(categoryId, data)).unwrap();
      } else {
        await dispatch(addCategory(data)).unwrap();
      }

      history.push("/category");
      notifySuccess("Data has been saved succesfully!");
    } catch (error) {}
  });

  useEffect(() => {
    console.log("current category id:", categoryId);
    if (categoryId) {
      dispatch(loadCategory(categoryId))
        .unwrap()
        .then(({ data: category }) => {
          reset(omit(category, "_id"));
        });
    }
  }, [categoryId]);

  return (
    <form onSubmit={onSubmit}>
      {!isUndefined(error) && (
        <div className="alert alert-danger mb-0 py-1 mb-2">{error}</div>
      )}
      <div className="mb-3">
        <label htmlFor="" className="form-label">
          Name:
        </label>
        <input className="form-control" {...register("name")} />
        {errors?.name?.type && (
          <span className="text-danger text-error">
            {errors?.name?.type === "string.empty" && "Title is required."}
            {errors?.name?.type === "string.min" &&
              `Title should not be less than ${DEFAULT_MIN_LENGTH} characters.`}
          </span>
        )}
      </div>
      <div className="mb-3">
        <label htmlFor="" className="form-label">
          Description:
        </label>
        <textarea
          className="form-control"
          {...register("description")}
        ></textarea>
        {errors?.description?.type && (
          <span className="text-danger text-error">
            {errors?.description?.type === "string.max" &&
              `Description should not be more than ${DEFAULT_MAX_LENGTH} characters.`}
          </span>
        )}
      </div>
      <div className="col-auto">
        <button type="submit" className="btn me-2" disabled={processing}>
          {processing && <Loading />} Submit
        </button>
        <Link to={CATEGORY_ROOT} className="btn">
          Back
        </Link>
      </div>
    </form>
  );
};

export default CategoryForm;
