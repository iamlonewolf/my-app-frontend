import { FC } from "react";
import { NavLink } from "react-router-dom";

export interface SidebarProps {}
const Sidebar: FC<SidebarProps> = () => {
  return (
    <nav
      id="sidebarMenu"
      className="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse"
    >
      <div className="position-sticky pt-3">
        <ul className="nav flex-column">
          <li className="nav-item">
            <NavLink className="nav-link" to="/dashboard">
              <i className="fas fa-home me-2"></i>
              Dashboard
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/bookmark">
              <i className="fas fa-bookmark me-2"></i>
              <span>Bookmark</span>
            </NavLink>
          </li>
          {/* <li className="nav-item">
            <NavLink className="nav-link" to="/notes">
              <i className="fas fa-file-signature me-2"></i>
              <span>Notes</span>
            </NavLink>
          </li> */}
          <li className="nav-item">
            <NavLink className="nav-link" to="/category">
              <i className="fas fa-list-alt me-2"></i>
              <span>Categories</span>
            </NavLink>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Sidebar;
