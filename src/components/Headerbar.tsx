import { FC, Fragment } from "react";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { hasIdentity } from "../selectors/auth";
import { authLoggedOut } from "../store/auth";

export interface HeaderbarProps {}

const Headerbar: FC<HeaderbarProps> = () => {
  const dispatch = useDispatch();
  const isLoggedIn = useSelector(hasIdentity);
  return (
    <Fragment>
      <header className="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
        <Link className="navbar-brand col-md-3 col-lg-2 me-0 px-3" to="/">
          My App
        </Link>
        <button
          className="navbar-toggler position-absolute d-md-none collapsed"
          type="button"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="navbar-nav flex-row">
          {!isLoggedIn && (
            <Fragment>
              <div className="nav-item text-nowrap">
                <Link className="nav-link px-3" to="/auth/login">
                  Login
                </Link>
              </div>
              <div className="nav-item text-nowrap">
                <Link className="nav-link px-3" to="/auth/register">
                  Register
                </Link>
              </div>
            </Fragment>
          )}
          {isLoggedIn && (
            <Fragment>
              <div className="nav-item text-nowrap">
                <Link className="nav-link account-link" to="/profile">
                  <i className="fas fa-user-circle"></i>
                </Link>
              </div>
              <div className="nav-item text-nowrap">
                <Link
                  onClick={() => dispatch(authLoggedOut())}
                  className="nav-link px-3"
                  to="/"
                >
                  Logout
                </Link>
              </div>
            </Fragment>
          )}
        </div>
      </header>
    </Fragment>
  );
};

export default Headerbar;
