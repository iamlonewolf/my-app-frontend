import { FC } from "react";

export interface NotFoundProps {}

const NotFound: FC<NotFoundProps> = () => {
  return (
    <div className="pt-3">
      <h1 className="h2">Page not found</h1>
    </div>
  );
};

export default NotFound;
