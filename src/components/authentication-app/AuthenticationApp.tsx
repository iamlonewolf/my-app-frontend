import "./AuthenticationApp.scss";
import { Redirect, RouteComponentProps } from "react-router-dom";
import { includes } from "lodash-es";

import Authentication, {
  AuthenticationMode,
} from "./../../common/authentication/Authentication";
import { hasIdentity } from "../../selectors/auth";
import { useSelector } from "react-redux";

export interface AuthenticationAppProps extends RouteComponentProps<any> {
  test?: string;
}

const AuthenticationApp: React.FC<AuthenticationAppProps> = ({ match }) => {
  const isLoggedin = useSelector(hasIdentity);
  const { mode } = match.params;
  if (
    !includes([AuthenticationMode.LOGIN, AuthenticationMode.REGISTRATION], mode)
  ) {
    return <Redirect to="/not-found" />;
  }

  if (isLoggedin) {
    return <Redirect to="/dashboard" />;
  }
  return (
    <div className="row d-flex align-items-center h-100 p-0">
      <div className="col-xl-6 col-xxl-5 col-lg-6 col-md-10 offset-md-1 col-sm-12 offset-lg-3 auth-wrap pt-3 pb-4 px-4 rounded">
        <Authentication mode={mode} redirect="/dashboard" />
      </div>
    </div>
  );
};

export default AuthenticationApp;
