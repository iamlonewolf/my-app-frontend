import { FC, Fragment } from "react";

export interface DashboardAppProps {}

const DashboardApp: FC<DashboardAppProps> = () => {
  return (
    <Fragment>
      <div className="pt-3">
        <h1>Welcome to MyApp</h1>
        <div>
          An app that has some basic features like bookmarking and saving notes.
        </div>
      </div>
    </Fragment>
  );
};

export default DashboardApp;
