import { FC, Fragment, lazy } from "react";
import {
  Link,
  Redirect,
  Route,
  RouteComponentProps,
  Switch,
} from "react-router-dom";
import "./BookmarkerApp.scss";
import { hasIdentity } from "./../../selectors/auth";
import { useSelector } from "react-redux";
import { isRouteMatch } from "./../../utilities/route";
import {
  AUTH_LOGIN,
  BOOKMARK_ADD,
  CATEGORY_ROOT,
  CATEGORY_ADD,
  CATEGORY_EDIT,
  BOOKMARK_EDIT,
  BOOKMARK_ROOT,
} from "./../../routes";
import BookmarkListFilter from "./bookmark-list-filter/BookmarkListFilter";

const bookmarkList = lazy(() => import("./bookmark-list/BookmarkList"));
const bookmarkForm = lazy(() => import("./bookmark-form/BookmarkForm"));
const bookmarkCategorForm = lazy(
  () => import("../category-app/category-form/CategoryForm")
);
const bookmarkCategoryList = lazy(
  () => import("../category-app/category-list/CategoryList")
);

export interface BookmarkerAppProps extends RouteComponentProps<any> {}

const BookmarkerApp: FC<BookmarkerAppProps> = ({ location }) => {
  const isLoggedIn = useSelector(hasIdentity);
  return (
    <Fragment>
      {!isLoggedIn && <Redirect to={AUTH_LOGIN} />}
      <div className="bookmarker-app">
        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
          <h1 className="h4 d-flex">
            <Link to={BOOKMARK_ROOT} className="text-dark">
              Bookmarker
            </Link>
            {isRouteMatch(location.pathname, CATEGORY_ROOT) && (
              <div className="ms-2">&#62;Categories</div>
            )}
            {isRouteMatch(location.pathname, BOOKMARK_ADD) && (
              <div className="ms-2">&#62;Add</div>
            )}
            {isRouteMatch(location.pathname, BOOKMARK_EDIT) && (
              <div className="ms-2">&#62;Edit</div>
            )}
          </h1>
          <div>
            <BookmarkListFilter />
          </div>
        </div>
        <Switch>
          <Route path={BOOKMARK_ROOT} exact component={bookmarkList} />
          <Route path={BOOKMARK_ADD} exact component={bookmarkForm} />
          <Route path={BOOKMARK_EDIT} exact component={bookmarkForm} />
          <Route path={CATEGORY_EDIT} exact component={bookmarkCategorForm} />
          <Route path={CATEGORY_ADD} exact component={bookmarkCategorForm} />
          <Route path={CATEGORY_ROOT} exact component={bookmarkCategoryList} />
          <Redirect to={BOOKMARK_ROOT} />
        </Switch>
      </div>
    </Fragment>
  );
};

export default BookmarkerApp;
