import "./BookmarkListFilter.scss";
import { FC, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import queryString from "query-string";
import { Link, RouteComponentProps, withRouter } from "react-router-dom";
import { BOOKMARK_ADD, BOOKMARK_ROOT } from "../../../routes";
import { getBookmarkListCount } from "../../../selectors/bookmark";
import { valueToNumber } from "../../../utilities/num";
import { DEFAULT_PAGE_SIZE } from "../../../models/common";
import CategorySelector from "../../category-app/category-selector/CategorySelector";
import { BookmarkParams } from "../../../models/interfaces/params/bookmarkParams";
import { getAccountId } from "./../../../selectors/auth";

export interface BookmarkListFilterProps extends RouteComponentProps<any> {}

const BookmarkListFilter: FC<BookmarkListFilterProps> = ({
  location,
  history,
}) => {
  const queryParams: BookmarkParams = queryString.parse(location.search);
  const query = queryParams;
  const bookmarksCount = useSelector(getBookmarkListCount);
  const accountId = useSelector(getAccountId);
  const [searchText, setSearch] = useState("");
  const page = valueToNumber(queryParams?.page, 0);
  const totalPage = Math.ceil(bookmarksCount / DEFAULT_PAGE_SIZE);

  useEffect(() => {
    setSearch(queryParams?.searchText ?? "");
  }, [queryParams?.searchText]);

  const goPrev = () => {
    query.page = page - 1;
    history.push(`${BOOKMARK_ROOT}?${queryString.stringify(query)}`);
  };

  const goNext = () => {
    query.page = page + 1;
    history.push(`${BOOKMARK_ROOT}?${queryString.stringify(query)}`);
  };

  const onSubmit = (event: any) => {
    event.preventDefault();
    query.searchText = searchText;
    query.page = 0;
    history.push(`${BOOKMARK_ROOT}?${queryString.stringify(query)}`);
  };

  const onSelectCategory = (category: string) => {
    console.log("CATEGORY >>>>>>", category);
    query.category = category;
    if (!category) {
      query.category = undefined;
    }
    history.push(`${BOOKMARK_ROOT}?${queryString.stringify(query)}`);
  };

  return (
    <div className="bookmark-list-filter d-flex align-items-center">
      {bookmarksCount > 0 && (
        <div className="d-flex me-2 rounded-pill px-3 py-1 bookmark-count">
          <span className="me-2">Total:</span>
          <span>{bookmarksCount}</span>
        </div>
      )}
      <Link className="btn btn-sm me-2" to={BOOKMARK_ADD}>
        <i className="fas fa-plus"></i>
      </Link>
      <form
        onSubmit={(event) => onSubmit(event)}
        className="input-group input-group-sm  me-2"
      >
        <input
          type="text"
          className="form-control"
          value={searchText}
          onChange={(e) => setSearch(e.target.value)}
        />
        <button className="btn" type="submit">
          <i className="fas fa-search"></i>
        </button>
      </form>
      <CategorySelector
        onSelect={(e) => onSelectCategory(e)}
        accountId={accountId}
        categoryId={queryParams?.category}
        className="me-2"
      />
      {bookmarksCount > DEFAULT_PAGE_SIZE && (
        <div className="btn-group">
          <button
            onClick={() => goPrev()}
            className="btn btn-sm"
            disabled={!(page > 0)}
          >
            <i className="fas fa-chevron-left"></i>
          </button>
          <button
            onClick={() => goNext()}
            className="btn btn-sm"
            disabled={!(page < totalPage - 1)}
          >
            <i className="fas fa-chevron-right"></i>
          </button>
        </div>
      )}
    </div>
  );
};

export default withRouter(BookmarkListFilter);
