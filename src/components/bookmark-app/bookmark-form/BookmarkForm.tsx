import "./BookmarkForm.scss";
import { FC, Fragment, useEffect, useState } from "react";
import { joiResolver } from "@hookform/resolvers/joi";
import { useForm } from "react-hook-form";
import {
  bookmarkSchema,
  DEFAULT_MAX_LENGTH,
  DEFAULT_MIN_LENGTH,
} from "../../../models/schemas/bookmark";
import {
  addBookmark,
  loadBookmark,
  updateBookmark,
} from "../../../store/bookmark";
import { useDispatch, useSelector } from "react-redux";
import { Bookmark } from "../../../models/interfaces/bookmark";
import { isLoading, getBookmarkError } from "../../../selectors/bookmark";
import Loading from "../../../common/loading";
import { isUndefined, omit } from "lodash-es";
import { AppDispatch } from "../../../store/configureStore";
import { Link, RouteComponentProps, useParams } from "react-router-dom";
import CategorySelector from "../../category-app/category-selector/CategorySelector";
import { notifySuccess } from "./../../../common/notify";
import { getAccountId } from "./../../../selectors/auth";

export interface BookmarkFormProps extends RouteComponentProps<any> {
  bookmarkId?: string;
}

const BookmarkForm: FC<BookmarkFormProps> = ({ history }) => {
  const TOTAL_ROWS = 3;
  const dispatch = useDispatch<AppDispatch>();
  const loading = useSelector(isLoading);
  const error = useSelector(getBookmarkError);
  const accountId = useSelector(getAccountId);

  const [processing, setProcessing] = useState(false);
  const [currentBookmark, setBookmark] = useState<Bookmark | undefined>(
    undefined
  );
  const { bookmarkId } = useParams<{ bookmarkId: string }>();

  const resolver = joiResolver(bookmarkSchema);
  const {
    register,
    handleSubmit,
    reset,
    setValue,
    formState: { errors },
  } = useForm({ resolver });

  useEffect(() => {
    console.log("current bookmark id:", bookmarkId);
    if (bookmarkId) {
      dispatch(loadBookmark(bookmarkId))
        .unwrap()
        .then(({ data: bookmark }) => {
          bookmark = omit(bookmark, "_id");
          setBookmark(bookmark);
          reset({ ...bookmark, category: bookmark?.category?._id });
        });
    }
  }, [dispatch]);

  const onSubmit = handleSubmit(async (data) => {
    try {
      data = { ...data, category: data?.category };
      setProcessing(true);
      if (bookmarkId) {
        await dispatch(updateBookmark(bookmarkId, data)).unwrap();
      } else {
        await dispatch(addBookmark(data)).unwrap();
      }
      history.push("/bookmark");
      notifySuccess("Data has been saved succesfully!");
    } catch (error) {
    } finally {
      setProcessing(false);
    }
  });

  const selectCategory = (categoryId: string) => {
    setValue("category", categoryId);
    setBookmark({ ...currentBookmark, category: { _id: categoryId } });
  };

  return (
    <Fragment>
      {loading && <Loading />}
      {!loading && (
        <form onSubmit={onSubmit}>
          {!isUndefined(error) && (
            <div className="alert alert-danger mb-0 py-1 mb-2">{error}</div>
          )}
          <div className="mb-3">
            <label htmlFor="" className="form-label">
              Title:
            </label>
            <input className="form-control" {...register("title")} />
            {errors?.title?.type && (
              <span className="text-danger text-error">
                {errors?.title?.type === "string.empty" && "Title is required."}
                {errors?.title?.type === "string.min" &&
                  `Title should not be less than ${DEFAULT_MIN_LENGTH} characters.`}
              </span>
            )}
          </div>
          <div className="mb-3">
            <label htmlFor="" className="form-label">
              URL:
            </label>
            <textarea
              className="form-control"
              {...register("url")}
              rows={TOTAL_ROWS}
            ></textarea>
            {errors?.url?.type && (
              <span className="text-danger text-error">
                {errors?.url?.type === "string.empty" && "URL is required."}
                {errors?.url?.type === "string.min" &&
                  `URL should not be less than ${DEFAULT_MIN_LENGTH} characters.`}
              </span>
            )}
          </div>
          <div className="mb-3">
            <label htmlFor="" className="form-label">
              Description:
            </label>
            <textarea
              className="form-control"
              {...register("description")}
              rows={TOTAL_ROWS}
            ></textarea>
            {errors?.description?.type && (
              <span className="text-danger text-error">
                {errors?.description?.type === "string.max" &&
                  `Description should not be more than ${DEFAULT_MAX_LENGTH} characters.`}
              </span>
            )}
          </div>
          <div className="mb-3">
            <label htmlFor="" className="form-label">
              Category:
            </label>
            <CategorySelector
              categoryId={currentBookmark?.category?._id}
              accountId={accountId}
              onSelect={(categoryId) => selectCategory(categoryId)}
            />
          </div>
          <div className="col-auto">
            <button type="submit" className="btn me-2">
              {processing && <Loading />} Submit
            </button>
            <Link to="/bookmark" className="btn">
              Back
            </Link>
          </div>
        </form>
      )}
    </Fragment>
  );
};

export default BookmarkForm;
