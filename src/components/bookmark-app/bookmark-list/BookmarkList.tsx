import "./BookmarkList.scss";
import { useState, FC, useEffect } from "react";
import { isEmpty } from "lodash-es";
import queryString from "query-string";
import { format } from "date-fns";
import RemoveConfirmationModal from "../../../common/remove-confirmation-modal/removeConfirmationModal";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch } from "../../../store/configureStore";
import { deleteBookmark, loadBookmarks } from "./../../../store/bookmark";
import { getAccountId } from "../../../selectors/auth";
import { getBookmarks, isLoading } from "./../../../selectors/bookmark";
import { Bookmark } from "../../../models/interfaces/bookmark";
import { Link, RouteComponentProps } from "react-router-dom";
import { notifyError } from "../../../common/notify";
import { notifySuccess } from "./../../../common/notify";
import { valueToNumber } from "../../../utilities/num";
import { DEFAULT_PAGE, DEFAULT_PAGE_SIZE } from "../../../models/common";
import Loading from "../../../common/loading";

export interface BookmarkerListProps extends RouteComponentProps<any> {}

const BookmarkerList: FC<BookmarkerListProps> = ({ location }) => {
  const queryParams = queryString.parse(location.search);
  const dispatch = useDispatch<AppDispatch>();
  const accountId = useSelector(getAccountId);
  const bookmarks = useSelector(getBookmarks);
  const loading = useSelector(isLoading);
  const [show, showModal] = useState(false);
  const [removing, showRemoveLoading] = useState(false);
  const [currentCategory, selectCategory] = useState<Bookmark | undefined>(
    undefined
  );

  useEffect(() => {
    initializeBookmarks();
  }, [queryParams?.page, queryParams?.searchText, queryParams.category]);

  const initializeBookmarks = () => {
    const page = valueToNumber(queryParams?.page, DEFAULT_PAGE);
    let params = {
      page: page,
      pageSize: DEFAULT_PAGE_SIZE,
      searchText: queryParams?.searchText,
      category: queryParams?.category,
    };
    dispatch(loadBookmarks(accountId, params));
  };

  const confirmRemove = async () => {
    try {
      showRemoveLoading(true);
      await dispatch(deleteBookmark(currentCategory?._id)).unwrap();
      initializeBookmarks();
      notifySuccess("Category has been deleted.");
    } catch (err) {
      notifyError("Failed deleting this category.");
    } finally {
      showModal(false);
      showRemoveLoading(false);
    }
  };

  const remove = (bookmark: Bookmark) => {
    showModal(true);
    selectCategory(bookmark);
  };

  const getDateFormat = (date: string | undefined) => {
    if (!date) {
      return undefined;
    }
    return format(new Date(date), "LLLL dd, yyyy");
  };

  return (
    <div className="bookmark-list">
      {loading && <Loading />}
      {!loading && isEmpty(bookmarks) && <div className="py-2">No records</div>}
      {!loading && !isEmpty(bookmarks) && (
        <div className="col-table">
          <div className="col-head">
            <div className="col-row">
              <div className="col-item px-2">Title</div>
              <div className="col-item px-2">URL</div>
              <div className="col-item px-2">Category</div>
              <div className="col-item px-2">Date Added</div>
              <div className="col-item px-2"></div>
            </div>
          </div>
          <div className="col-body">
            {bookmarks?.map((bookmark: Bookmark) => (
              <div key={bookmark?._id} className="col-row py-2">
                <div className="col-item d-flex align-items-center px-2">
                  {bookmark?.title}
                </div>
                <div className="col-item d-flex align-items-center px-2 url">
                  <Link to={{ pathname: bookmark?.url }} target="_blank">
                    {bookmark?.url}
                  </Link>
                </div>
                <div className="col-item d-flex align-items-center px-2">
                  {bookmark?.category?.name}
                </div>
                <div className="col-item d-flex align-items-center px-2">
                  {getDateFormat(bookmark?.createdAt)}
                </div>
                <div className="col-item d-flex align-items-center px-2">
                  <div>
                    <Link
                      to={`/bookmark/${bookmark?._id}/edit`}
                      className="btn btn-sm btn-action me-2"
                    >
                      <i className="fas fa-edit"></i>
                    </Link>
                    <button
                      onClick={() => remove(bookmark)}
                      className="btn btn-sm btn-danger btn-action"
                    >
                      <i className="fas fa-trash-alt"></i>
                    </button>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      )}
      <RemoveConfirmationModal
        show={show}
        centered={true}
        loading={removing}
        onSubmit={() => confirmRemove()}
        onClose={() => showModal(false)}
      />
    </div>
  );
};

export default BookmarkerList;
