// dashboard routes
export const DASHBOARD_ROOT = "/dashboard";
export const PROFILE_ROOT = "/root";

// not found route
export const NOT_FOUND_ROOT = "/not-found";

// bookmark routes
export const BOOKMARK_ROOT = "/bookmark";
export const BOOKMARK_ADD = "/bookmark/add";
export const BOOKMARK_EDIT = "/bookmark/:bookmarkId/edit";

// category routes
export const CATEGORY_ROOT = "/category";
export const CATEGORY_ADD = "/category/add";
export const CATEGORY_EDIT = "/category/:categoryId/edit";

// auth routes
export const AUTH_ROOT = "/auth/:mode";
export const AUTH_LOGIN = "/auth/login";
