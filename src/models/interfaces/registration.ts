import { Account } from "./account";
export interface RegistrationData extends Account {
  name?: string;
  email: string;
  password: string;
}
