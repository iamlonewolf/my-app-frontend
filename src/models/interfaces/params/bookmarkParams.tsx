export interface BookmarkParams {
  page?: number;
  pageSize?: number;
  searchText?: string;
  category?: string;
}
