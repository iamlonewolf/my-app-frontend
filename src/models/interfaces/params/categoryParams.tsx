export interface CategoryParams {
  page?: number;
  pageSize?: number;
  searchText?: string;
}
