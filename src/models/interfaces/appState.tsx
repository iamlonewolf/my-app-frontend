export interface AppState {
  data?: any;
  list?: any[];
  count?: number;
  loading?: boolean;
  error?: any;
}
