import { AppState } from "./appState";
import { Account } from "./account";

export interface AuthState extends AppState {
  identity?: Account;
}
