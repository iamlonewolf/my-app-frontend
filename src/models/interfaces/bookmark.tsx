import { Category } from "./category";
export interface Bookmark {
  _id?: string;
  title?: string;
  url?: string;
  description?: string;
  category?: Category | undefined;
  createdAt?: string;
  updatedAt?: string;
}
