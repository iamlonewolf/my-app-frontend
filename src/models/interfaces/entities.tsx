import { AppState } from "./appState";

export interface Entities {
  bookmark?: AppState;
  account?: AppState;
  category?: AppState;
}
