import { Entities } from "./entities";
import { AuthState } from "./authState";

export interface RootState {
  entities?: Entities;
  auth?: AuthState;
}
