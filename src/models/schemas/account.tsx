import Joi from "joi";
import { Account } from "../interfaces/account";
export const DEFAULT_MAX_LENGTH = 255;
export const DEFAULT_MIN_LENGTH = 5;

export const accountSchema = Joi.object<Account>({
  name: Joi.string().required().min(DEFAULT_MIN_LENGTH),
  email: Joi.string().email({ tlds: { allow: false } }),
  password: Joi.string().required().min(DEFAULT_MIN_LENGTH),
});

export const loginSchema = Joi.object<Account>({
  email: Joi.string().email({ tlds: { allow: false } }),
  password: Joi.string().required().min(DEFAULT_MIN_LENGTH),
});
