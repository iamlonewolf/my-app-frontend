import Joi from "joi";
import { Category } from "../interfaces/category";

export const DEFAULT_MAX_LENGTH = 500;
export const DEFAULT_MIN_LENGTH = 1;
export const categorySchema = Joi.object<Category>({
  _id: Joi.string().allow(""),
  name: Joi.string().required().max(DEFAULT_MAX_LENGTH).min(DEFAULT_MIN_LENGTH),
  description: Joi.string().max(DEFAULT_MAX_LENGTH).allow(""),
});
