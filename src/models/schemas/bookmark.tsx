import Joi from "joi";
import { Bookmark } from "../interfaces/bookmark";

export const DEFAULT_MAX_LENGTH = 500;
export const DEFAULT_MIN_LENGTH = 5;
export const bookmarkSchema = Joi.object<Bookmark>({
  title: Joi.string()
    .required()
    .max(DEFAULT_MAX_LENGTH)
    .min(DEFAULT_MIN_LENGTH),
  url: Joi.string().required().max(DEFAULT_MAX_LENGTH).min(DEFAULT_MIN_LENGTH),
  description: Joi.string().max(DEFAULT_MAX_LENGTH).allow(""),
  category: Joi.alternatives().try(Joi.object(), Joi.string()).allow(""),
  _id: Joi.string().allow(""),
  createdAt: Joi.string().allow(""),
  updatedAt: Joi.string().allow(""),
});
