import { matchPath } from "react-router-dom";

export const isRouteMatch = (currentRoute: string, route: string) => {
  return matchPath(currentRoute, { path: route, exact: true, strict: false });
};
