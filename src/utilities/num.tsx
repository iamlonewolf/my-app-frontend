import { isNaN, isUndefined, toNumber } from "lodash-es";

export const valueToNumber = (value: any, defaultValue: number): number => {
  if (isUndefined(value) || isNaN(toNumber(value)) || value?.trim() === "") {
    return defaultValue;
  }
  return toNumber(value);
};

export const rangeLimitGuard = (
  value: number,
  min: number,
  max: number
): number => {
  if (value > max) {
    return max;
  }
  if (value < min) {
    return min;
  }
  return value;
};
